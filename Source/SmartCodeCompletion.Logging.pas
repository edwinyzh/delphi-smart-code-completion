{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

unit SmartCodeCompletion.Logging;

interface

{$I SmartCodeCompletion.inc}

uses
  ToolsAPI,
  Windows;

{$IFDEF DEBUG}
procedure Trace(const name: string; const args: string = '');
{$ENDIF}

implementation

{$IFDEF DEBUG}
procedure Trace(const name: string; const args: string = '');
var
  s: string;
begin
  s := 'Trace: ' + name + '(' + args + ')';
  OutputDebugString(PChar(s));
  if (GetKeyState(VK_SCROLL) and 1 = 1) then
  begin
    (BorlandIDEServices as IOTAMessageServices).AddToolMessage('', s, 'SmartCC',
      0, 0);
  end;
end;
{$ENDIF}

end.
